#!/bin/bash

set -eu

source "$(dirname "$0")/utils.sh"

log_with_header "Updating chart README.md"

setup_git $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

title "Running helm-docs"
helm-docs -c charts -o ../../README.md
success "Successfully updated README.md"

title "Committing and pushing changes"
if (git diff --quiet && git diff --staged --quiet); then
  warn "No changes detected, skipping ..."
  exit 0
else
  info "Committing changes"
  git add README.md
  git commit -m "Update README.md"

  info "Pushing changes"
  git push origin $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME

  success "Changes committed and pushed"
fi
