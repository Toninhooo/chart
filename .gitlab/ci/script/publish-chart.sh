#!/bin/bash

set -euo pipefail

source "$(dirname "$0")/utils.sh"

if [ "${1}" == "pre" ]; then
  chart_tgz="dependabot-gitlab-pre.tgz"
  mv dependabot-gitlab-*.tgz "$chart_tgz"
else
  chart_tgz="$(echo dependabot-gitlab-*.tgz)"
fi

log_with_header "Publish helm chart"
gsutil -o "Credentials:gs_service_key_file=${GCLOUD_KEYFILE}" cp "$chart_tgz" gs://dependabot-gitlab
